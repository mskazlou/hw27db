import mysql.connector as mysql
from mysql.connector import Error


class SqlDatabase:
    host_name = 'localhost'
    user_name = 'root'
    user_password = "password"

    @staticmethod
    def create_connection(db_name=None):

        my_connection = None

        try:

            my_connection = mysql.connect(
                host=SqlDatabase.host_name,
                user=SqlDatabase.user_name,
                passwd=SqlDatabase.user_password,
                database=db_name
            )
            print('Connection to MySQL DB successful')

        except Error as e:
            print(f"The error '{e}' occurred")

        return my_connection

    @staticmethod
    def create_database(connection, query):
        cursor = connection.cursor()

        try:
            cursor.execute(query)
            print('Database created successfully')

        except Error as e:
            print(f"The error '{e} occurred")

    @staticmethod
    def execute(connection, query):
        cursor = connection.cursor()
        cursor.execute(query)
        print(cursor.fetchall())

    @staticmethod
    def insert_many(connection, query, values):
        cursor = connection.cursor()
        cursor.executemany(query, values)
        connection.commit()
        print(cursor.rowcount, 'records inserted')


hw_db = SqlDatabase()
hw_connect = hw_db.create_connection(db_name='test_db')

# запрос для создания таблицы
create_hw_db_query = """CREATE TABLE test_orders (id INT AUTO_INCREMENT PRIMARY KEY,
                                       ord_no INT(11),
                                       purch_amt DEC(7, 2),
                                       ord_date DATE,
                                       customer_id INT(11),
                                       salesman_id INT(11))"""
# hw_db.create_database(hw_connect, create_hw_db_query)
#
# запрос для наполнения таблицы значениями
hw_db_insert_query = """INSERT INTO test_orders (ord_no, purch_amt, ord_date, customer_id, salesman_id)
                        VALUES (%s, %s, %s, %s, %s)"""
#
values = [
    ('70010', '150.5', '2012-10-05', '3005', '5002'),
    ('70009', '270.65', '2012-09-10', '3001', '5005'),
    ('70002', '65.26', '2012-10-05', '3002', '5001'),
    ('70004', '110.5', '2012-08-17', '3009', '5003'),
    ('70007', '948.5', '2012-09-10', '3005', '5002'),
    ('70005', '2400.6', '2012-07-27', '3007', '5001'),
    ('70008', '5760', '2012-09-10', '3002', '5001'),
    ('70010', '1983.43', '2012-10-10', '3004', '5006'),
    ('70003', '2480.4', '2012-06-27', '3008', '5003'),
    ('70012', '250.45', '2012-06-27', '3008', '5002'),
    ('70011', '75.29', '2012-08-17', '3003', '5007'),
    ('70013', '3045.6', '2012-04-25', '3002', '5001')
]
# hw_db.insert_many(hw_connect, hw_db_insert_query, values)

# проверка что база заполнилась
# query = 'SELECT * FROM test_orders'
# hw_db.execute(hw_connect, query)


first_task_query = """SELECT ord_no, ord_date, purch_amt
                      FROM test_orders WHERE salesman_id = 5002;"""
# hw_db.execute(hw_connect, first_task_query)

second_task_query = """SELECT DISTINCT(salesman_id) FROM test_orders"""

second_task_query_extra = """SELECT salesman_id FROM test_orders
                             GROUP BY salesman_id"""
# hw_db.execute(hw_connect, second_task_query)

# вывод данных в порядке возрастания даты
third_task_query = """SELECT ord_date, purch_amt, ord_no, salesman_id
                      FROM test_orders
                      ORDER BY ord_date ASC """
# hw_db.execute(hw_connect, third_task_query)

fourth_task_query = """SELECT id, ord_no 
                       FROM test_orders
                       WHERE ord_no BETWEEN 70001 AND 70007"""
# hw_db.execute(hw_connect, fourth_task_query)
